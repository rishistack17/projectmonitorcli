package serializer

import "encoding/json"

func Serailize( i interface{})([]byte,error){
	doc,err:=json.MarshalIndent(i,"","   ")
	if err!=nil{
		return []byte{},err
	}
	return doc,nil
}

func Deserialize(arry []byte,i interface{}) (interface{},error) {
	err:= json.Unmarshal(arry,i)
	if err!=nil {
		return i,err
	}
	return i,nil
}