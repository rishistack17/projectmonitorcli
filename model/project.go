package model

import (
	gid "github.com/google/uuid"
	"gitlab.com/rishistack17/projectmonitorcli/database"
)

type Project struct {
	ID          string `json:"id,omitempty"`
	UserName    string `json:"user_name,omitempty"`
	Name        string `json:"name,omitempty"`
	Description string `json:"description"`
	Url         string `json:"url,omitempty"`
	Info 		Info 	`json:"info,omitempty"`
	Status 		string `json:"status,omitempty"`
	Git  		Git  `json:"git,omitempty"`
}
func (p *Project)GetItAId(){
	p.ID=gid.New().String()
}
func (p *Project) AddaNewProject(db *database.Db) {
	id := gid.New().String()
	db.CreateAnewDoc(p, id)

}
