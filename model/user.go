package model

import "gitlab.com/rishistack17/projectmonitorcli/database"

type User struct {
	Name     string            `json:"name"`
	Password string            `json:"password"`
	Projects map[string]string `json:"projects"`
}

func (u *User)AddAnewUser(db *database.Db){
	db.CreateAnewDoc(u,u.Name)
}