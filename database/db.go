package database

import (
	"fmt"

	"github.com/dgraph-io/badger/v3"
	"gitlab.com/rishistack17/projectmonitorcli/serializer"
)

type Db struct {
	DB  *badger.DB
	Url string
}

func NewConnection(url string) (*Db, error) {
	connection, err := badger.Open(badger.DefaultOptions(url))
	if err != nil {
		return &Db{}, err
	}
	return &Db{
		connection,
		url,
	}, nil
}

func (db *Db) CreateAnewDoc(i interface{}, key string) {
	db.DB.Update(func(txn *badger.Txn) error {
		if _, err := txn.Get([]byte(key)); err == badger.ErrKeyNotFound {
			toInsert, err := serializer.Serailize(i)
			if err != nil {
				return err
			}
			e := badger.NewEntry([]byte(key),toInsert)
			err = txn.SetEntry(e)
			fmt.Println(err)
		} else {
			fmt.Println("key is already present ")
			return nil
		}
		return nil
		// e := badger.NewEntry([]byte(key),toInsert)
		// err := txn.SetEntry(e)
		// return nil
	})
}

func (db *Db) GetAValue(i interface{}, key string) string {
	var res []byte
	db.DB.Update(
		func(txn *badger.Txn) error {
			if _, err := txn.Get([]byte(key)); err == badger.ErrKeyNotFound {
				fmt.Println("invalid key is not in our database")
			} else {
				val, _ := txn.Get([]byte(key))
			
				err := val.Value(func(val []byte) error {
					res = append(res, val[:]...)
					return nil
				})
				return err
			}
			return nil
		},
	)
	return string(res)
}

func (db *Db)UpdateAdoc(i interface{},key string) error {
	
	err :=  db.DB.Update(func(txn *badger.Txn) error {
		if _, err := txn.Get([]byte(key)); err == badger.ErrKeyNotFound {
			fmt.Println("o such doc with given key")
			return badger.ErrKeyNotFound
		}else{
			toInsert,err :=serializer.Serailize(i)
			fmt.Printf("error in serialization %+v\n",err)
			e := badger.NewEntry([]byte(key),toInsert)
			err = txn.SetEntry(e)
			return err
		}
	})
	return err
}