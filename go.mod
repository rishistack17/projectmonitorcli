module gitlab.com/rishistack17/projectmonitorcli

go 1.16

require (
	github.com/dgraph-io/badger/v3 v3.2011.1 // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.7 // direct
)
